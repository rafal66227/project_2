import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class GUI extends JFrame  implements ActionListener 
{

	private JButton bZapisz, bZakoncz;
	private JLabel lPodajKwote, lPodajWartosc,lPodajOkres, lWyswietlRate;
	private JTextField tKwota, tWartosc, tOkres;
	public double kwota, wartosc, okres, oprocentowanie, wynik, liczbaRat;

	
	   
	public  GUI(){
		
	
		setSize(1000,600);
		setTitle("Kalkulator rat kredytu");
		setLayout(null);
		getContentPane().setBackground(new Color(100, 100, 255));
		
		//////////////////////////////////////////////////
		
		bZapisz = new JButton("Zapisz!");
		bZapisz.setBounds(550, 100, 100, 40);
		add(bZapisz);
		bZapisz.addActionListener(this);

		bZakoncz = new JButton("Zako�cz!");
		bZakoncz.setBounds(670, 100, 100, 40);
		add(bZakoncz);
		bZakoncz.addActionListener(this);

		//////////////////////////////////////////////////

		lPodajKwote = new JLabel("Podaj kwot� kredytu(z�):");
		lPodajKwote.setBounds(30, 50, 250, 25);
		lPodajKwote.setForeground(new Color(255,255,255));
		lPodajKwote.setFont(new Font("Comic Sans MS", Font.BOLD + Font.ITALIC, 15));
		add(lPodajKwote);
		
		tKwota = new JTextField("");
		tKwota.setBounds(300, 50, 150, 24);
		add(tKwota);
		
		//////////////////////////////////////////////////	
		
		lPodajWartosc = new JLabel("Podaj warto�� nieruchomo�ci(z�):");
		lPodajWartosc.setBounds(30, 100, 250, 25);
		lPodajWartosc.setForeground(new Color(255,255,255));
		lPodajWartosc.setFont(new Font("Comic Sans MS", Font.BOLD + Font.ITALIC, 15));
		add(lPodajWartosc);
		
		tWartosc = new JTextField("");
		tWartosc.setBounds(300, 100, 150, 24);
		add(tWartosc);
		
		//////////////////////////////////////////////////	
		
		lPodajOkres = new JLabel("Podaj okres sp�aty kredytu (lata):");
		lPodajOkres.setBounds(30, 150, 250, 25);
		lPodajOkres.setForeground(new Color(255,255,255));
		lPodajOkres.setFont(new Font("Comic Sans MS", Font.BOLD + Font.ITALIC, 15));
		add(lPodajOkres);
		
		tOkres = new JTextField("");
		tOkres.setBounds(300, 150, 150, 24);
		add(tOkres);
		
        

		//////////////////////////////////////////////////
		
		lWyswietlRate = new JLabel();
		lWyswietlRate.setBounds(30, 200, 500, 24);
		lWyswietlRate.setForeground(new Color(231,203,12));
		lWyswietlRate.setFont(new Font("Arial", Font.BOLD, 18));
		add(lWyswietlRate);
		
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		Object source = e.getSource();
		
		
		
		if(source == bZapisz){
			
			JFileChooser fc = new JFileChooser();
			if (fc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION)
			
			{
			
			boolean done = false;
			while(!done){
			try{
			kwota = Double.parseDouble(tKwota.getText());
			wartosc = Double.parseDouble(tWartosc.getText());
			okres = Double.parseDouble(tOkres.getText());
			done = true;
			}
			catch(NumberFormatException nfe) {
				lWyswietlRate.setText("Nie poda�e� wymaganych danych!");
				
			}
			
			}
			
			
			liczbaRat = okres * 12;
			double raty[] = new double [(int) (liczbaRat)];
			
			if (wartosc <= (kwota*0.7)) oprocentowanie = 0.06;
			if (wartosc > (kwota*0.7) && wartosc <= (kwota*0.9)) oprocentowanie = 0.05;
			if (wartosc > (kwota*0.9) && wartosc <= kwota ) oprocentowanie = 0.04;
			
			for(int i = 0; i < liczbaRat; i++){
				
			wynik = kwota/liczbaRat*(1+(liczbaRat - i)*oprocentowanie/12);
			wynik *= 100;
			wynik = Math.round(wynik);
			wynik /= 100;
			
			 raty[i] = wynik;
			}
			
			PrintWriter pw = null;
			
			try {
				
				pw = new PrintWriter(fc.getSelectedFile());
				
				for(int i = 0; i < liczbaRat; i++ ){
				pw.println(i+1 + ". rata wynosi: " + raty[i] + "\n");
				
				}
				
				pw.close();
				
			}
			
			catch (FileNotFoundException e1) {
				
				e1.printStackTrace();
				
			}
			

			lWyswietlRate.setText("List� rat wpisano do pliku!");
			
		}
			
		}
		
		if(source == bZakoncz) dispose();
		
		
	}
	

}
